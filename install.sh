#!/bin/bash
red=$(tput setaf 1)
green=$(tput setaf 2)
normal=$(tput sgr0)

# Get the arguments
arguments=()
for i in $@; do 
	arguments+=("$i")
done

sudo printf "\nStarting Installer\n"

# Check existence of .bash_aliases
if ! [ -f ~/.bash_aliases ]; then
	printf "\n${green}Creating ~/.bash_aliases${normal}\n"
	touch ~/.bash_aliases
fi

# Add ~/.local/bin to PATH variable
HOME=$(echo ~)
mkdir -p ~/.local/bin
if grep -Fq "/.local/bin" ~/.bash_aliases; then
	printf "${green}~/.local/bin already in \$PATH${normal}\n"
	# Quite an assumption. But I had to go with something
else
	printf "${green}Adding ~/.local/bin to \$PATH${normal}\n"
	printf "\nexport PATH=\$PATH:$HOME/.local/bin\n" >> ~/.bash_aliases
fi

# Update packages. Installation of packages occur in most tools
sudo apt-get update
if [ $? -ne 0 ]; then
	printf "\n${red}Failed to update packages${normal}\n"
	exit 1
fi

if [[ " ${arguments[@]} " =~ " alloy " ]]; then
	printf "${green}\n____Installing Alloy____${normal}\n"
	cd Alloy
	chmod u+x install_alloy.sh
	./install_alloy.sh
	if [ $? -ne 0 ]; then
		printf "\n${red}Failed to install Alloy${normal}\n"
		exit 1
	fi
	cd ..
fi

if [[ " ${arguments[@]} " =~ " spin " ]]; then
	printf "\n${green}\n____Installing Spin____${normal}\n"
	cd Spin
	chmod u+x install_spin.sh
	./install_spin.sh
	if [ $? -ne 0 ]; then
		printf "\n${red}Failed to install Spin${normal}\n"
		exit 1
	fi
	cd ..
fi

if [[ " ${arguments[@]} " =~ " rodin " ]]; then
	printf "\n${green}\n____Installing Rodin____${normal}\n"
	cd Rodin
	chmod u+x install_rodin.sh
	./install_rodin.sh
	if [ $? -ne 0 ]; then
		printf "\n${red}Failed to install Rodin${normal}\n"
		exit 1
	fi
	cd ..
fi

if [[ " ${arguments[@]} " =~ " vcc " ]]; then
	printf "\n${green}\n____Installing VCC and JPF____${normal}\n"
	cd VCC_JPF
	chmod u+x install_vcc_jpf.sh
	./install_vcc_jpf.sh
	if [ $? -ne 0 ]; then
		printf "\n${red}Failed to install VCC and JPF${normal}\n"
		exit 1
	fi
	cd ..
fi

if [[ " ${arguments[@]} " =~ " afl " ]]; then
	printf "\n${green}\n____Installing AFL____${normal}\n"
	cd AFL
	chmod u+x install_afl.sh
	./install_afl.sh
	if [ $? -ne 0 ]; then
		printf "\n${red}Failed to install AFL${normal}\n"
		exit 1
	fi
	cd ..
fi