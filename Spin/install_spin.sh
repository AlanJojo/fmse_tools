#!/bin/bash
red=$(tput setaf 1)
green=$(tput setaf 2)
normal=$(tput sgr0)

# Install wish
sudo apt-get -y install wish
if [ $? -ne 0 ]; then
	printf "\n${red}Failed to install wish${normal}\n"
	exit 1
fi

printf "\n${green}Preparing files...${normal}\n"
chmod ugo+x ispin.tcl
chmod ugo+x spin
HOME=$(echo ~)
sed -i -e "s|user|$HOME|g" spin.desktop

if [ -d $HOME/.local/programs/spin ]; then
	printf "\n${green}Deleting Spin folder from previous installation...\n${normal}"
	rm -rf $HOME/.local/programs/spin
fi

printf "\n${green}Copying required files...${normal}\n"
mkdir -p $HOME/.local/share/applications
mkdir -p $HOME/.local/programs/spin
yes | cp spin.desktop $HOME/.local/share/applications/
yes | cp spin $HOME/.local/programs/spin/
yes | cp spin.png $HOME/.local/programs/spin/
yes | cp ispin.tcl $HOME/.local/programs/spin/

printf "\n${green}Creating link to executable in bin ...\n${normal}"
ln -sf $HOME/.local/programs/spin/spin $HOME/.local/bin/spin
ln -sf $HOME/.local/programs/spin/ispin.tcl $HOME/.local/bin/ispin

exit 0