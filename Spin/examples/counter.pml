byte count = 2;

proctype counter() {
  do
  :: true -> count = (count + 1) % 4;
  :: true -> {    /* !([] ((! ((count==1))) || (X ((count==2))))) */
	do
	:: (! ((! ((count==1))))) -> goto accept_S0
	od;
accept_S0:
	do
	:: atomic { (! (((count==2)))) -> assert(!(! (((count==2))))) }
	od;
accept_all:
	skip
};
  od
}

init {
  run counter();
}
 
ltl prop1 { [] (count <= 3) }
ltl inc { [] ((count == 1) -> X(count == 2)) }
ltl prop2 { [] ((count!=3) -> (((count == 0) || (count == 1)) U (count == 2))) }
