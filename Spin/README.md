# Spin
Spin is a widely used open-source software verification tool. The tool can be used for the formal verification of multi-threaded software applications.

Spin Website: [https://spinroot.com](https://spinroot.com/spin/whatispin.html)

License: [SPIN Commercial License](http://www.spinroot.com/spin/spin_license.html)

## Usage
After installation iSpin will produce a desktop application which can be used to run the GUI for Spin.

Assuming that you have an alloy file named `counter.pml` in the current folder, run the following command in terminal to load this file in iSpin:
```bash
ispin counter.pml
```
Note: Filename is optional (if you do not want to load any file in GUI)

Or, if you want to run Spin from the command line, run any of the following as required:

```bash
# Simulate random execution till termination or assertion violation
spin counter.pml
```

```bash
# Simulate with random seed 100
spin -n 100 counter.pml
```

```bash
# Simulate with process state info (-p), values of global variables (-g), values of local variables (-l), verbose (-w) info
spin -p -g -w counter.pml
```

```bash
# Generate and Compile analyser (pan.c)
spin -a counter.pml
gcc pan.c -o counter

# Run analyser for ltl property "inc"
./counter -a -N inc

# Execute the counterexample trace in counter.pml.trace
spin -t -g -p counter.pml
```

## Disclaimer
I **DO NOT OWN** any of the software given in this repository. I have just created installation scripts for easy installation.