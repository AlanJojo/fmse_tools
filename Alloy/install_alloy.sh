#!/bin/bash
green=$(tput setaf 1)
green=$(tput setaf 2)
normal=$(tput sgr0)

JAVA_LOC=$(which java)
if [ -z "$JAVA_LOC" ]; then
	printf "\n${green}Installing Java...${normal}"
	sudo apt-get -y install openjdk-11-jre
	if [ $? -ne 0 ]; then
		printf "\n${red}Failed to install Java 11${normal}\n"
		exit 1
	fi
fi

printf "\n${green}Preparing files...\n${normal}"
HOME=$(echo ~)
JAVA_LOC=$(which java)
sed -i -e "s|user|$HOME|g" alloy.desktop
sed -i -e "s|user|$HOME|g" alloy.sh
sed -i -e "s|java_location|$JAVA_LOC|g" alloy.sh

if [ -d $HOME/.local/programs/alloy ]; then
	printf "\n${green}Deleting Alloy folder from previous installation...\n${normal}"
	rm -rf $HOME/.local/programs/alloy
fi

printf "\n${green}Copying required files...\n${normal}"
chmod u+x alloy.sh

mkdir -p $HOME/.local/share/applications
mkdir -p $HOME/.local/programs/alloy
yes | cp alloy.desktop $HOME/.local/share/applications/
yes | cp alloy* $HOME/.local/programs/alloy/

printf "\n${green}Creating link to executable in bin ...\n${normal}"
ln -sf $HOME/.local/programs/alloy/alloy.sh $HOME/.local/bin/alloy

exit 0