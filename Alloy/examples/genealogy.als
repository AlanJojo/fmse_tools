module test

abstract sig Person {spouse: lone Person, parents: set Person}
sig Man, Woman extends Person {}

fact Parenthood {

    -- every person has a mother and father
    all p: Person | one mother: Woman | one father: Man |
        p.parents = mother + father
    }

fact SocialNorms {
    -- spouse is symmetric
    //spouse = ~spouse
    -- a man's spouse is a woman and vice versa
    Man.spouse in Woman && Woman.spouse in Man
    -- can't marry a sibling
    no p: Person | some p.spouse.parents & p.parents
    -- can't marry a parent
    no p: Person | some p.spouse & p.parents
    -- parents are married
    all p: Person | p.parents.spouse = p.parents
    }

pred Show {}

run Show for 7
