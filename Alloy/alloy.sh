#!/bin/bash

# Set the path to the Java runtime environment
JAVA_LOC=java_location

# Set the path to the Alloy Analyzer JAR file
ALLOY_JAR=user/.local/programs/alloy/alloy-6.0.0.jar

# Run the Alloy Analyzer with appropriate options
"$JAVA_LOC" -jar "$ALLOY_JAR" "$@"