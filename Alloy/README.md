# Alloy
Alloy is an open source language and analyzer for software modeling. Alloy is a product of the Software Design Group at MIT.

Alloy Website: [https://alloytools.org](https://alloytools.org)

## Usage
After installation, Alloy will produce a desktop application which can be used to run the GUI for Alloy.

Assuming that you have an alloy file named `genealogy.als` in the current folder, run the following command in terminal to load this file in Alloy:
```bash
alloy genealogy.als
```
Note 1: Filename is optional (if you do not want to load any file in GUI)

Note 2: This is an alias for the following command:
- java -jar ~/.local/programs/alloy/alloy-6.0.0.jar <filename.als>

## Disclaimer
I **DO NOT OWN** any of the software given in this repository. I have just created installation scripts for easy installation.