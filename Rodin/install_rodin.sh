#!/bin/bash
red=$(tput setaf 1)
green=$(tput setaf 2)
normal=$(tput sgr0)

# Install Java 11 if Java doesn't exist
if type -p java; then
	printf "\nFound java executable in \$PATH\n"
	_java=java
else
	printf "Installing java"
	sudo apt-get install -y openjdk-11-jre
	if [ $? -ne 0 ]; then
		printf "\n${red}Failed to install Java 11${normal}\n"
		exit 1
	fi
fi

function ver { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }

# Find Java version
version=$("$_java" -version 2>&1 | awk -F '"' '/version/ {print $2}')
printf "\nversion \"$version\""

if [ $(ver $version) -gt $(ver 11) ]; then
	printf "Current Java version is greater than 11"
else
	printf "Installing Java version 11"
	sudo apt-get install -y openjdk-11-jre
fi

printf "\n${green}Changing permission for rodin folder...${normal}\n"
USER=$(whoami)
GROUP=$(id -gn)
chown -R $USER:$GROUP rodin

printf "\n${green}Preparing files...${normal}\n"
chmod ugo+x rodin/rodin
HOME=$(echo ~)
sed -i -e "s|user|$HOME|g" rodin.desktop

if [ -d $HOME/.local/programs/alloy ]; then
	printf "\n${green}Deleting Rodin folder from previous installation...\n${normal}"
	rm -rf $HOME/.local/programs/rodin
fi

printf "\n${green}Copying required files...${normal}\n"
mkdir -p $HOME/.local/share/applications
mkdir -p $HOME/.local/programs/rodin
yes | cp rodin.desktop $HOME/.local/share/applications/
yes | cp -a rodin/ $HOME/.local/programs/

printf "\n${green}Creating link to executable in bin ...\n${normal}"
ln -sf $HOME/.local/programs/rodin/rodin $HOME/.local/bin/rodin

exit 0