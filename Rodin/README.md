# Rodin
The Rodin Platform is an Eclipse-based IDE for Event-B that provides effective support for refinement and mathematical proof. The platform is open source, contributes to the Eclipse framework and is further extendable with plugins. 

Rodin Website: [https://www.event-b.org](https://www.event-b.org/index.html)

## Usage
After installation, Rodin will produce a desktop application which can be used to run the GUI for Rodin.

While installing Rodin, make sure than you do not have Java version less than 11 as atleast Java 11 is required to run Rodin.

The following command will launch Rodin from terminal.
```bash
rodin
```
You can select the workspace and start working.

Note: If you feel that the current theme of Rodin reduces visibility, you can go to Window->Preferences to get the Preferences dialog box. In the dialog box, go to General->Appearance and change the Theme from there.

## Disclaimer
I **DO NOT OWN** any of the software given in this repository. I have just created installation scripts for easy installation.