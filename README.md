# Installation script for tools required for E0 272 FMSE course (IISc Bangalore)

To install, run the following commands on a terminal:
```bash
git clone https://gitlab.com/AlanJojo/fmse_tools.git
cd fmse_tools
chmod u+x install.sh
./install.sh alloy spin rodin vcc afl
```

Note: VCC and JPF come together in a single docker image. Running VCC installation script will give you JPF also inside the same docker container.

If you want to install only specific tools, mention only those tools while running install.sh

e.g.: If you want to install only VCC and JPF, type the last command as follows:
```bash
./install.sh vcc
```

**Note**: After installation, some tools may require you to reload bash (Simple way to achieve this: Close current terminal and launch another one) or reboot your system for you to access the tool.

## Usage
The usage explanation for all tools are given in the README files in their respective folders.

After installation, Alloy, iSpin and Rodin will produce desktop applications which can be used to run the GUI of these applications.

## Testing
The script has been tested and works fine for the following distributions:
- Linux Mint 21.3
- Ubuntu 22.04

## Disclaimer
I **DO NOT OWN** any of the software given in this repository. I have just created installation scripts for easy installation.

Course webpage: [https://www.csa.iisc.ac.in/~deepakd/fmse-2024/index.html](https://www.csa.iisc.ac.in/~deepakd/fmse-2024/index.html)