#!/bin/bash
red=$(tput setaf 1)
green=$(tput setaf 2)
normal=$(tput sgr0)

# See if make exists
MAKE_LOC=$(which make)
if [ -z "$MAKE_LOC" ]; then
	printf "\n${green}Installing make...${normal}"
	sudo apt-get -y install build-essential
	if [ $? -ne 0 ]; then
		printf "\n${red}Failed to install build-essential${normal}\n"
		exit 1
	fi
fi

# See if Python3 exists
PYTHON_LOC=$(which python3)
if [ -z "$PYTHON_LOC" ]; then
	printf "\n${green}Installing Python 3...${normal}"
	sudo apt-get -y install python3
	if [ $? -ne 0 ]; then
		printf "\n${red}Failed to install python3${normal}\n"
		exit 1
	fi
fi

cd mafl-2.35b
printf "\n${green}Preparing environment and files...\n${normal}"
HOME=$(echo ~)

printf "\n${green}Cleaning ...\n${normal}"
make clean
if [ $? -ne 0 ]; then
	printf "\n${red}\'make clean\' failed${normal}\n"
	exit 1
fi

printf "\n${green}Creating the required binaries ...\n${normal}"
make
if [ $? -ne 0 ]; then
	printf "\n${red}\'make\' failed${normal}\n"
	exit 1
fi

printf "\n${green}Installing the required binaries ...\n${normal}"
make install
if [ $? -ne 0 ]; then
	printf "\n${red}\'make install\' failed${normal}\n"
	exit 1
fi

chmod u+x afl-init.sh afl-xor.py afl-generate-tables.sh
cp afl-init.sh $HOME/.local/bin/afl-init
cp afl-xor.py $HOME/.local/bin/afl-xor
cp argparse.py $HOME/.local/bin/argparse.py
cp afl-generate-tables.sh $HOME/.local/bin/afl-generate-tables
make clean

cd ..
exit 0
