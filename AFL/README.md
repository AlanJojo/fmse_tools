# AFL
American fuzzy lop (AFL) is a security-oriented fuzzer that employs a novel type of compile-time instrumentation and genetic algorithms to automatically discover clean, interesting test cases that trigger new internal states in the targeted binary.

Github code for AFL: [https://github.com/google/AFL](https://github.com/google/AFL)

AFL Website: [https://lcamtuf.coredump.cx/afl/](https://lcamtuf.coredump.cx/afl/)

The modification to the AFL code was done by Raveendra Kumar M. of TCS Research. The modifications done to the original code are mentioned in the [Usage](#Usage) section. Thank you Mr. Raveendra Kumar M. for providing the modified code.

## Notes

The folder mafl-2.35b contains a slightly modified version of afl-gcc, that we have produced.  The changes are as follows:
- Code optimizations are disabled in this version of afl-gcc (these make the assembly code hard to understand)
- This version of afl-gcc emits a file random-loc.txt. This file contains information about trampolines, and is required by afl-xor

The naming of output files produced by afl-fuzz has been changed to ensure that the character "`:`" will not be part of the name. Instead, the character "`_`" will take its place. This is because some filesystems (like NTFS) do not support files with such characters.
    
We have also modified afl-showmap. We describe the modification later in the [Usage](#Usage) section.

You should use the tools in this folder only and should not directly download and install AFL from the AFL site nor install afl from apt package manager. (Doing so may override our modified programs)

## Usage
We are using an example program given inside the folder [ForClassWhile](ForClassWhile)
```bash
# Go into the folder "ForClassWhile"
cd ForClassWhile
```

1. Before running AFL, run the following command:
    ```bash
    source afl-init
    ```
    Under the hood, it executes the following commands:
    - export AFL_KEEP_ASSEMBLY=1
    
      This command basically instruct afl-gcc to not throw away the assembly language file that it produces.

    - export TMPDIR=.

      This makes the assembly language file's name begin with "." character. So, it will be a hidden file. The file will be present in the same directory where you run afl-gcc
      
      Note that if several assembly language files are present corresponding to your program, then you can ignore all but the most recent version.

    - export AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1
      
      Setting the flag above will cause crashing inputs to appear in the "hangs" sub-folder while running afl-fuzz

2. To run afl-gcc on the file `for-class-while.c`, run the following:
    ```bash
    afl-gcc for-class-while.c -o for-class-while
    ```

    In addition to producing the executable file `for-class-while`, this run also produces the file `random-loc.txt`, as well as the assembly language file (for human inspection).

    The manually created file `assembly-program-in-brief.s` describes the high-level structure of the assembly language file mentioned above. 

3. Before running afl-fuzz, you need to use
    ```bash
    sudo cpufreq-set
    ```
    to set the frequency governors of *all* your cores to the "performance" governor. (Some distributions of Ubuntu may have UI based controls to do the same thing.)

    If for some reason you are not able to do the step above, then run the following command:
    ```bash
    export AFL_SKIP_CPUFREQ=1
    ```

4. Make sure that the `In` folder contains the seed file(s) that you supply.

    For example, if you need your seed file to contain the input '`xx`', run the following command:
    ```bash
    echo -n "xx" > In/seed
    ```

    Run afl-fuzz as follows:
    ```bash
    afl-fuzz -i In -o Out ./for-class-while
    ```
    The `Out` folder will contain the output files produced by afl.

    The `queue` folder within the `Out` folder contains all the non-crashing non-hanging test inputs that were placed in the Q by afl-fuzz. Note that the name of each test input file in this directory encodes:
    - the sequence number of creation of this file (the "id" field)
    - the test input(s) that served as parents for this test input (the "src" field)
    - the mutation operation that was used to produce this input (the "op" field)
    - the position in the parent test input where the mutation was performed (the "pos" field)
    - the number of locations where the mutation was performed (the "rep" field, for non-deterministic mutations)

    The `hangs` folder within the `Out` folder will contain the test inputs that caused the program to hang (i.e., take more than 20  milliseconds to finish executing). This default timeout can be changed using an option of afl-fuzz.

    The `crashes` folder within the `Out` folder will contain the test inputs that caused the program to crash.

5. To see the final branch map table at the end of a run on a test input file `id_000000,orig_seed`, run the following command:
    ```bash
    afl-showmap -o "showmap-result.txt" ./for-class-while < Out/queue/id_000000,orig_seed
    ```
    We have modified afl-showmap compared to its original version. The output file `showmap-result.txt` will contain two columns.
    - The first column in the branch-pair index (i.e., the xor operation's result)
    - The second column shows the EXACT number of times this branch pair was traversed in the run (without any rounding off).
    
    Unvisited branch pairs are omitted. (The original version of afl-showmap shows the log to the base 2 of the rounded value of the visit count, not the exact visit count.)

6. To understand the contents of `showmap-result.txt`, you can run `afl-xor`. This command takes `random-loc.txt` and `showmap-result.txt` as arguments.
    ```bash
    afl-xor -o "Table.txt" -s "showmap-result.txt" -r "random-loc.txt"
    ```
    Note: Under the hood, afl-xor is a python script. So, you need python in your system to run this.

    The output from afl-xor is a file `Table.txt`. Each row in this table corresponds to a branch pair (i.e., to a unique pair of trampolines) in the assembly file.
    - "Prev Random" is the random number corresponding to the previous trampoline visited
    - "Curr Random" corresponds to the current trampoline being visited.
    - "Table Index" is the branch-pair index. That is, it is nothing but "Prev Random">>1 XOR "Curr Random", and serves as the index into the Shm array that is accessed and incremented during  the current visit to the trampoline.
    
    Note that the visit to the very first trampoline (where prev_random is zero) is not recorded in `Table.txt` (it is present in `showmap-result.txt`).

    Note that in case you re-compile your program using afl-gcc, a latest assembly language file (with a fresh set of random numbers in the trampolines will be produced). Therefore, to generate fresh results that are consistent with this assembly language file, you would need to run afl-showmap and afl-xor once again.

#### Optional - Under testing
A script named `afl-generate-tables.sh` is created to generate showmap-result.txt files and Table.txt files for all inputs in the queue folder.

This command takes the program name, location of `random-loc.txt`, and location of output folder as arguments. Make sure that the queue folder is present inside the output folder provided as argument.
```bash
afl-generate-tables -p for-class-while -r "random-loc.txt" -o Out/
```

This will generate files of the form `showmap-result-[id].txt` in the folder `Out/showmap/` and corresponding `Table-[id].txt` files in the folder `Out/table/`.

## Disclaimer
I **DO NOT OWN** any of the software given in this repository. I have just created installation scripts for easy installation.
