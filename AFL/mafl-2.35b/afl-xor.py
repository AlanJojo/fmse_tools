#!/usr/bin/env python3

from os import path
from operator import itemgetter
from argparse import ArgumentParser

# Setting color for terminal outputs
[normal, red, green] = ['\033[00m','\033[91m','\033[92m']

# Function to check if the file is valid and writes an error otherwise
def is_valid_file(parser, arg):
    if not path.exists(arg):
        parser.error("The file " + red + arg + normal + " does not exist!")
    else:
        return arg

# Initialize parser
parser = ArgumentParser(description="Branch pair table generator using" + green + " random-loc.txt " + normal + "file and" + green + " showmap-result.txt " + normal + "file")

# Find if arguments are provided and check for validity
parser.add_argument("-o", dest="out_file", metavar="FILENAME", help = "Specify output filename")
parser.add_argument("-s", dest="show_loc", metavar="LOCATION", type=lambda x: is_valid_file(parser, x), help = "Specify location of" + green + " showmap-result.txt " + normal + "file")
parser.add_argument("-r", dest="rand_loc", metavar="LOCATION", type=lambda x: is_valid_file(parser, x), help = "Specify location of" + green + " random-loc.txt " + normal + "file")

# Read arguments from command line
args = parser.parse_args()

if not args.out_file:
    # If no argument is provided, set default output filename
    out_file = "Table.txt"
else:
    out_file = args.out_file

if not args.show_loc:
    # If no argument is provided, check if "showmap-result.txt" is present in current directory
    is_valid_file(parser, "showmap-result.txt")
    show_loc = "showmap-result.txt"
else:
    show_loc = args.show_loc

if not args.rand_loc:
    # If no argument is provided, check if "random-loc.txt" is present in current directory
    is_valid_file(parser, "random-loc.txt")
    rand_loc = "random-loc.txt"
else:
    rand_loc = args.rand_loc

# Read all information from "random-loc.txt" file
with open(rand_loc) as f:
    data = f.readlines()

# Populate values for address and location
address = []            # Stores current address
prev_address = []       # Stores correspoding floor values of address[i] / 2
loc = []                # Stores location information
for i in range(len(data)):
    # Reason why odd lines is file satisfy the condition "i%2 == 0" :
    # Counting of i starts from 0 whereas line numbers in file start from 1
    words = data[i].split()
    if i%2 == 0:    # For odd lines in "random-loc.txt" (address)
        a = int(words[0])
        address.append(a)
        prev_address.append(a//2)   # Floor value
    else:           # For even lines in "random-loc.txt" (location)
        if len(words) < 6:
            loc.append(int(words[2]))
        else:
            loc.append(float(words[2]) + 0.1)
 
# Read all information from "showmap-result.txt" file
with open(show_loc) as f:
    data = f.readlines()

# Populate values for branch pair visited and frequency
trace_result = []
frequency = []
for line in data:
    words = line.split(':')
    trace_result.append(int(words[0]))  # Branch pair xor ed values
    frequency.append(int(words[1]))     # Frequency of visit

# Finding branch pairs visited
output_list = []
for m in range(len(trace_result)):  # For every branch pair mentioned in showmap-result.txt
    # See whether some value in address and some value in prev_address can be xor ed to get this value in showmap-result.txt
    # a^b = c also means that c^a = b
    # So instead of using two nested "for loops", one each for both arrays, we only use one for loop for address array
    # and check if the xor value of trace_result[m] and address[j] is present in prev_address array
    # If yes, then we have found the branch pair visited (prev_address, address)
    for j in range(len(address)):   # Go through all address values
        z = trace_result[m] ^ address[j]
        # Check if the xor value is present in prev_address
        if z in prev_address:
            i = prev_address.index(z)
            output_list.append([loc[i], loc[j], address[i], address[j], trace_result[m], frequency[m]])
# Note that will disregard exactly one value in showmap-result.txt as prev_address for that value is 0 (beginning of program)

# Sort the output_list
output_list.sort(key=itemgetter(0))

# Try opening output file for writing
try:
    f = open(out_file, 'w')
except OSError: # If output file can't be opened
    print("Could not open file " + red + out_file + normal + " for writing. Dumping output on terminal...\n")
    print("Prev Loc".ljust(11) + "Curr Loc".ljust(11) + "Prev Rand".ljust(12) + "Curr Rand".ljust(12) + "Table Index".ljust(15) + "Frequency".ljust(11) + "\n")
    for item in output_list:
        print(str(item[0]).ljust(11) + str(item[1]).ljust(11) + format(item[2],'x').ljust(12) + format(item[3],'x').ljust(12) + str(item[4]).ljust(15) + str(item[5]).ljust(11) + "\n")
    exit()

# If output file is opened, write in that file
f.write("Prev Loc".ljust(11) + "Curr Loc".ljust(11) + "Prev Rand".ljust(12) + "Curr Rand".ljust(12) + "Table Index".ljust(15) + "Frequency".ljust(11) + "\n")
for item in output_list:
    f.write(str(item[0]).ljust(11) + str(item[1]).ljust(11) + format(item[2],'x').ljust(12) + format(item[3],'x').ljust(12) + str(item[4]).ljust(15) + str(item[5]).ljust(11) + "\n")

# If program reaches here, everything happened successfully
print("Output has been written in file: " + green + out_file + normal)
