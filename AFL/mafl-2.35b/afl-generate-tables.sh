#!/bin/bash
# Script will generate showmap-result.txt files and Table.txt files for all inputs in the queue
red=$(tput setaf 1)
green=$(tput setaf 2)
normal=$(tput sgr0)

Help()
{
  # Display Help
  echo ""
  echo "Syntax: afl-generate-tables -p PROGRAM -r FILE -o OUTPUT_FOLDER_PATH"
  echo "Options:"
  echo "-p     Location of program to be run"
  echo "-r     Location of random-loc.txt file."
  echo "-o     Location of output folder (where queue is located)"
  echo "-h     Print this help."
  echo
}

program=""
rand_loc="random-loc.txt"
out=""

# Get the options
while getopts ":p:r:o:h" option; do
  case "${option}" in
    p) # Location of program to be run
      program="${OPTARG}";;
    r) # Location of random-loc.txt file
      rand_loc="${OPTARG}";;
    o) # Location of output folder
      out="${OPTARG}";;
    h) # display Help
      Help
      exit;;
    *) # Invalid option
      printf "\n${red}Error${normal}: Invalid option\n"
      Help
      exit;;
  esac
done

# Check if program is executable
if [ -f "${program}" ] && [ -x "${program}" ]; then
  printf "\nProgram: ${green}${program}${normal}"
else
	printf "\nNot executable: ${red}${program}${normal}\n"
  exit 1
fi

# Check existence of random-loc.txt
if [ -f ${rand_loc} ]; then
  printf "\nrandom-loc file: ${green}${rand_loc}${normal}"
else
	printf "\nFile not found: ${red}${rand_loc}${normal}\n\tGenerate this file by running afl-gcc\n"
	exit 1
fi

# Get the queue location
out=${out%/}
queue=${out}/queue
# Check existence of queue folder
if ! [ -d "${out}" ]; then
	printf "\nFolder not found: ${red}${out}${normal}\n"
  exit 1
elif ! [ -d "${queue}" ]; then
	printf "\nQueue folder not found: ${red}${queue}${normal}\n"
  exit 1
else
  printf "\nQueue folder: ${green}${queue}${normal}\n"
fi

# Create folders for storing showmap and tables
mkdir -p ${out}/showmap
mkdir -p ${out}/table

# Loop through input files in queue
for file in ${queue}/id*; do
  if [ -f "${file}" ]; then
    printf "\nFile: ${green}${file}${normal}\n"
    id=${file#*id_}
    id=${id%%,*}    # get queue file id

    printf "Generating showmap as: ${green}showmap-result-${id}.txt${normal}\n"
    afl-showmap -o "${out}/showmap/showmap-result-${id}.txt" "./${program}" < "${file}"

    printf "Running afl-xor\n"
    afl-xor -o "${out}/table/Table-${id}.txt" -s "${out}/showmap/showmap-result-${id}.txt" -r ${rand_loc}
  fi
done