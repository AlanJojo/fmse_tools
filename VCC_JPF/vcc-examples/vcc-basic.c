#include <vcc.h>
#include <limits.h>

void bar()
{
  unsigned int a, b;
  _(assume b < UINT_MAX);
  a = _(unchecked) (b + 1);
  _(assert a > b)
}

