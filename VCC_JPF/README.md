# VCC
VCC is a mechanical verifier for concurrent C programs. VCC is owned by Microsoft.

By running the installation script, you implicitly accept Microsoft's Terms and Conditions for Visual Studio and VCC. Please go through licenses at [MICROSOFT VISUAL STUDIO 2019 License Terms](https://go.microsoft.com/fwlink/?LinkId=2086102) and [VCC License](./VCC_wine/LICENSE). Run the script only if you agree with both the license agreements. 

Github code for VCC: [https://github.com/microsoft/vcc](https://github.com/microsoft/vcc)

# JPF

[Java PathFinder](https://travis-ci.org/javapathfinder/jpf-core) is an extensible software model checking framework for Java bytecode programs. All the latest developments, changes, documentation can be found on the
[wiki](https://github.com/javapathfinder/jpf-core/wiki) page. JPF is under Apache License. This license can be found in the GitHub page of jpf-core. 

[Symbolic PathFinder](https://github.com/SymbolicPathFinder/jpf-symbc) is a JPF extension that provides symbolic execution for Java bytecode. It performs a non-standard interpretation of byte-codes. SymbolicPathFinder is under Apache License. This license can be found in the GitHub page of jpf-core. 

Github code for jpf-core: [https://github.com/javapathfinder/jpf-core](https://github.com/javapathfinder/jpf-core)

Github code for jpf-symbc: [https://github.com/SymbolicPathFinder/jpf-symbc](https://github.com/SymbolicPathFinder/jpf-symbc)

The Dockerfile provided in JPF folder is a variant created from a Dockerfile provided by [Snigdha Athaiya](https://www.linkedin.com/in/snigdha-athaiya/) of Siemens Technology. Thank you Ms. Snigdha for showing how to install and run JPF on docker.

## Notes
VCC and JPF come together in a single docker image. Running VCC installation script will give you JPF also inside the same docker container.

If docker was not previously installed in your system and got installed while running this installation script, you may need to reload bash (Simple way to achieve this: Close current terminal and launch another one) or reboot your system for you to access docker.

## Usage
After installation a docker image named ```fmse:v1``` will be built and a container named ```vcc_jpf``` will be created from this image. To start this docker container, just run the following command:
```bash
docker start -i vcc_jpf
```
This will run the docker container and associate a pseudo tty for the user to interact with the container. You will enter the container as normal user in the folder **```/home/user```** where the following folders will be present:
- VCC - Sample VCC programs
- JPF - The jpf-core and jpf-symbc projects will be present inside this folder

### VCC
To run the file `vcc-basic.c` in VCC, run the following command
```bash
vcc VCC/vcc-basic.c
```

### JPF
While inside the docker container, if you want to run JPF on the random number example presented in class:
```bash
jpf JPF/jpf-core/src/examples/Rand.jpf
```
You should be able to see 1 error reported by JPF in the example.

Before running jpf-symbc, we need to build the Java files using ant. You can do any one of these:

1. Either stay in the home directory itself and give the following ant build command:
    ```bash
    ant -buildfile JPF/jpf-symbc/build.xml
    ```
2. Or go into `JPF/jpf-symbc` folder, build using ant and then come back:
    ```bash
    cd JPF/jpf-symbc
    ant
    cd ../..
    ```

To run jpf-symbc, give the command:
```bash
jpf JPF/jpf-symbc/src/examples/simple/Branches.jpf
```

### Creating new file
Vim is installed in the container. You can create a new file `new.c` and start editing it by giving the command:
```bash
vim new.c
```

Another way is to install VS Code, add the docker extension available in it, and then modify the files.


## Disclaimer
I **DO NOT OWN** any of the software given in this repository. I have just created installation scripts for easy installation.