/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * Symbolic Pathfinder (jpf-symbc) is licensed under the Apache License, 
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package simple;

import gov.nasa.jpf.symbc.Debug;

public class Branches {

	

	/*Basic exploration and test creation */
	public static void simpleBranch(int x){
		if(x<0){
			x = -x;				
		}
		System.out.println("x >= 0");	
	}


	/*Exploration with asserts
	 * Tests will try to fail the assert as well
	 */
	public static void simpleBranchWithAssert(int x){
		if(x<0){
			x = -x;				
		}

		//this will generate a failing test case		
		//assert(x>0);

		//all passing test cases will be generated
		assert(x>=0);	
	}

	/*Exploration with asserts
	 * Test cases with return value displayed
	 */
	public static int simpleBranchWithAssertandReturn(int x){
		if(x<0){
			x = -x;				
		}
		assert(x>=0);	
		return x;
	}


	/*Larger example with infeasible paths pruning
	 * Try with Debug.getSymbolicIntegerValue and Debug.getSolvedPC
	 */
	public static void branch(int x, int y) {
		if (x < 0) {
			x = -x;
		}
		if (y < 0) {
			y = -y;
		}
		if (x < y) {
			System.out.println("abs(x)<abs(y)");
		} else if (x == 0) {
			System.out.println("x==y==0");
		} else {
			System.out.println("x>=y>=0");
		}		
	}


	public static void main(String[] args) {
		simpleBranch(1);
		//simpleBranchWithAssert(1);
		//simpleBranchWithAssertandReturn(1);
		//branch(1, 2);
		
	}

}
