/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * Symbolic Pathfinder (jpf-symbc) is licensed under the Apache License, 
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package NodeSimple;

import gov.nasa.jpf.symbc.Debug;
import gov.nasa.jpf.symbc.Symbolic;

public class NodeSimple {

	//@Symbolic("true")
	int elem;
    NodeSimple next;
  
	
    public NodeSimple(int x) {
    	elem = x;
    	next = null;
    }
	

    /*Simple Choices Exploration */
    public static void test1 (NodeSimple n, NodeSimple m) {                                           
        if(n==null  && m ==null)                                               
          System.out.println("Problem");                                                                                 
    } 

    /*Example that takes care of aliasing while exploration */
    /*Choices creation according to rule - older symbolic objects, null, new symbolic object */    
    public static void test2(NodeSimple n, NodeSimple m) {
    	if(n!=null && m!=null)
      System.out.println("Problem");    
    }


    /*Lazy initialization*/
    public static void test3(NodeSimple n, NodeSimple m) {
    	if (n == null || m == null) return;
    	n.next = m;
      if (n.next.next == null){
    		System.out.println("Problem!");
      }        
    }

    /*Lazy initialiation and handling of expression with two references */
    /*Large set of states explored due to combinatorial explosion */
    public static void test4(NodeSimple n, NodeSimple m, NodeSimple t) {
    	if (n == null || m == null) return;
    	n.next = m;
      Debug.printSymbolicRef(n,"Value of n" );
      if (n.next.next == t){
    		System.out.println("Problem!");
      }        
    }
    
    /* */
    public static void test5(NodeSimple n){
      if(n != null){
        if(n.next != null){
            System.out.println("next is not null");
          }
        else {
            System.out.println("next is null");
          }
        }      
    }

    /*Interplay of two choice generators - PCChoiceGenerator and HeapChoiceGenerator
     * Exploration will proceed normally.
     * Will show error while displaying test cases,
     * due to incompatibility between the two explorers - PC and Heap
     */
    public static void test6(NodeSimple n){
      if(n != null){
        if(n.next != null){
          if(n.elem > 0)
          {
            System.out.println("Value is positive");
          }
          else {
            System.out.println("value is zero or negative");
          }
        }
      }
    }

    /*Non-terminating example
     * One choice forces non-termination
     * stop execution using ctrl+c
     */
    public static void test7(NodeSimple n){
      for(NodeSimple curr = n; curr!=null; curr = curr.next){
        if(curr.elem < 0){
          System.out.println("problem");
          break;
        }
      }
    }
    
    
	public static void main(String[] args) {	
		
		NodeSimple X = new NodeSimple(5);
        
		test1(X,X);
    //test2(X,X);
    //test3(X,X);
    //test4(X);
    //test5(X);
    //test6(X);
    //test7(X);
	}

}
