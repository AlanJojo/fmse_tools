#!/bin/bash
red=$(tput setaf 1)
green=$(tput setaf 2)
normal=$(tput sgr0)

# See if docker is installed
DOCKER_LOC=$(which docker)
if [ -z "$DOCKER_LOC" ]; then
	# Install docker
	printf "\n${green}Installing Docker ...\n${normal}"
	sudo apt-get -y install docker.io
	if [ $? -ne 0 ]; then
		printf "\n${red}Failed to install docker${normal}\n"
		exit 1
	fi
else
	# Removing old image and container, if any exist
	if [ $( docker ps -a | grep container_vcc_jpf | wc -l ) -gt 0 ]; then
		printf "\n${green}Removing existing docker container \'container_vcc_jpf\' ...\n${normal}"
		docker rm container_vcc_jpf
	fi
	if [ -n "$(docker images -q fmse:v1 2> /dev/null)" ]; then
		printf "\n${green}Removing existing docker image \'fmse:v1\' ...\n${normal}"
		docker image rm fmse:v1
	fi
fi

printf "\n${green}Adding user to docker group ...\n${normal}"
sudo usermod -aG docker $USER

# Preparing to unzip the files
printf "\n${green}Unzipping required files ...\n${normal}"
tar -xzf jpf_files.tar.gz -C ./
tar -xzf wine_files.tar.gz -C ./
tar -xzf VCC.tar.gz -C ./
tar -xzf VC_1.tar.gz -C ./
tar -xzf VC_2.tar.gz -C ./

# Creating docker container
printf "\n${green}Building fmse Docker Image ...\n${normal}"
sg docker -c "docker build -t fmse:v1 ."
printf "\n${green}Creating Docker Container ...\n${normal}"
sg docker -c "docker create --name vcc_jpf -it fmse:v1"

printf "\n${green}You now have a docker container named \'vcc_jpf\'\n${normal}"
printf "\n${green}Run this docker container using the command: \'docker start -i vcc_jpf\'\n${normal}"

# Removing unzipped files
printf "\n${green}Removing temporary files ...\n${normal}"
rm -rf jpf-core/ jpf-symbc/ VC/ VCC/ wine/

exit 0