FROM eclipse-temurin:8-jdk-jammy

# JPF setup along with vim and nano
RUN apt-get update \
    && apt-get install -y ant vim nano \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# VCC setup
RUN dpkg --add-architecture i386 \
    && apt-get update -y \
    && apt-get install -y --no-install-recommends wine wine32 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Creating user
RUN useradd -ms /bin/bash user

# Switch to user mode
USER user

# Create JPF folder
RUN mkdir /home/user/JPF
WORKDIR /home/user/JPF

# Add JPF files
COPY --chown=user:user jpf-core /home/user/JPF/jpf-core/
COPY --chown=user:user jpf-symbc /home/user/JPF/jpf-symbc/
COPY --chown=user:user jpf-examples/simple /home/user/JPF/jpf-symbc/src/examples/simple/
COPY --chown=user:user jpf-examples/NodeSimple /home/user/JPF/jpf-symbc/src/examples/symbolicheap/

# Setup JPF environment
RUN mkdir /home/user/.jpf
RUN echo 'jpf-core = /home/user/JPF/jpf-core' >> /home/user/.jpf/site.properties
RUN echo 'jpf-symbc = /home/user/JPF/jpf-symbc' >> /home/user/.jpf/site.properties
RUN echo 'extensions=${jpf-core},${jpf-symbc}' >> /home/user/.jpf/site.properties

# Build JPF
WORKDIR /home/user/JPF/jpf-core
RUN ./gradlew buildJars
WORKDIR /home/user/JPF/jpf-symbc
RUN ant

# Setup JPF path
ENV JPF_HOME=/home/user/JPF
ENV JAVA_HOME=/opt/java/openjdk/
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$JPF_HOME/jpf-symbc/lib

# Setting JPF alias for easier access
RUN printf 'alias jpf="java -jar /home/user/JPF/jpf-core/build/RunJPF.jar"\n' >> /home/user/.bashrc

# Initializing wine for VCC
WORKDIR /home/user
RUN WINARCH=win32 wineboot --init
# Copying required VCC files
COPY --chown=user:user wine/mono /home/user/.wine/drive_c/windows/mono/
COPY --chown=user:user wine/system.reg /home/user/.wine/
COPY --chown=user:user wine/user.reg /home/user/.wine/
COPY --chown=user:user VCC /home/user/.wine/drive_c/VCC
COPY --chown=user:user VC /home/user/.wine/drive_c/VC
COPY --chown=user:user vcc-examples /home/user/VCC

# Setting VCC alias for easier access
RUN printf 'alias vcc="WINEDEBUG=-all wine /home/user/.wine/drive_c/VCC/vcc/Host/bin/Release/vcc.exe"\n' >> /home/user/.bashrc

# Set bash as docker entrypoint
ENTRYPOINT /bin/bash
